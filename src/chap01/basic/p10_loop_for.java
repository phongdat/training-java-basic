package chap01.basic;

public class p10_loop_for {
    public static void main(String[] args) {
        // For
//        for(int i = 0; i <= 10; i++) {
//            System.out.println(i + " -java not difficult");
//        }

        // While
//        int j = 1;
//        while (j <= 10) {
//            System.out.println(j + " -java not difficult");
//            j++;
//        }
        // Do While
        // => thuc hien noi dung trong vong lap truoc roi moi den dieu kien
        int k = 10;
        do {
            System.out.println(k + " -java not difficult");
            k++;
        }while (k < 10);
    }
}
