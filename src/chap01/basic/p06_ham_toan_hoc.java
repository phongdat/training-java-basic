package chap01.basic;

public class p06_ham_toan_hoc {
    public static void main(String[] args) {
        int numberOne = 15;
        int numberTwo = 10;
        double numberThree = 123.145;

        int maxResult = Math.max(numberOne, numberTwo);
        int minResult = Math.min(numberOne, numberTwo);

        System.out.println("max: " + maxResult);
        System.out.println("min: " + minResult);
        System.out.println("Ceil: " + Math.ceil(numberThree));//lam tron len
        System.out.println("Floor: " + Math.floor(numberThree));//lam tron xuong
        System.out.println("Round: " + Math.round(numberThree)); //lam tron' dung' gan nhat'

        double randomNumber = Math.random();//lay ra so thap phan tu 0 den 1;
        System.out.println("randomNumber: " + randomNumber);
    }
}
