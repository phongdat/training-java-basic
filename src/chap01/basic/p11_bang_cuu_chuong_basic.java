package chap01.basic;

public class p11_bang_cuu_chuong_basic {
    public static void main(String[] args) {
        int number = 3;
//        for (int i = 1; i <= 10; i++) {
//            // cach 1
//            // System.out.println(number + " X " + i + " = " + number * i);
//            //cach 2
//            System.out.printf("%d x %d = %d %n", number, i, number * i);
//        }
//        int j = 1;
//        while (j <= 10) {
//            System.out.printf("%d x %d = %d %n", number, j, number * j);
//            j++;
//        }
        int k = 1;
        do {
            System.out.printf("%d x %d = %d %n", number, k, number * k);
            k++;
        } while (k <= 10);
    }
}
