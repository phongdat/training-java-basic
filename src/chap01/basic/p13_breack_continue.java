package chap01.basic;

public class p13_breack_continue {
    public static void main(String[] args) {
        for(int i = 1; i <= 20; i++) {
            if(i % 7 == 0) {
                System.out.println("Ok: " + i);
                continue;
                //break; Nhay ra khoi vong lap
            }
            System.out.println(i);

            /* // chua co continue
                6
                Ok: 7
                7
             */
            /* // co continue => lenh continue se bo qua cac lenh phia duoi no', va lap tiep cac gia tri khac
                6
                Ok: 7
                8
             */
        }
    }
}
