package chap01.basic;

public class p12_bang_cuu_chuong_advance {
    public static void main(String[] args) {
        int i;
        for(i = 2; i <= 9; i++) {
            System.out.println("Bang cuu chuong " + i);
            for(int j = 1; j <= 10; j++) {
                System.out.printf("%d x %d = %d %n", i, j, i * j);
            }
        }
    }
}
