package chap01.basic;

public class p05_toan_tu {
    public static void main(String[] args) {
        int numberStart = 20;
        int numberEnd = 0;
        /*
         * number-- trả về giá trị của number.
         * sau đó giảm number xuống 1 đơn vị
         */
        numberEnd = numberStart--;
        System.out.println("Number start: " + numberStart);
        System.out.println("Number End: " + numberEnd);
    }
    public static void main01(String[] args) {
        int numberStart = 20;
        int numberEnd = 0;
        /*
         * number++ trả về giá trị của number.
         * sau đó tăng number lên 1 đơn vị
         */
        numberEnd = numberStart++;
        System.out.println("Number start: " + numberStart);
        System.out.println("Number End: " + numberEnd);
    }
}
