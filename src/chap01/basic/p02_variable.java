package chap01.basic;

public class p02_variable {
    public static void main(String[] args) {
        /*
        * register variable
         */
        int age  = 30;
        int year = 1990;
        char name = 'n';
        String fullName = "Tran Van Dat";
        System.out.println(age);
        System.out.println(year);
        System.out.println(name);
        System.out.println(fullName);
    }
}
