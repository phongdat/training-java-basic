package chap01.basic;

public class p04_data_type {
    public static void main(String[] args) {
        // Kieu du lieu trong java duoc chia thanh 2 loai
        //Kieu du lieu nguyen thuy(primitive): byte, short, int, long, float, double, boolean, char and String
        //Kieu du lieu tham chieu(reference): array, class, interface...vv
        int var1 = 20;
        int var2 = 10;
        boolean var3 = true;
        double var4 = 123.456;
        String fullName = "Tran Van Dat";
        System.out.println("var 1:" + var1);
        System.out.println("var 2:" + var2);
        System.out.println("var 3:" + var3);
        System.out.println("var 4:" + var4);
        System.out.println("var 4:" + fullName);

        int a = 10;
        double b = 1.23;
        int c = a + (int)b;
        double d = a + b;
        System.out.println("Ket qua c: " + c);
        System.out.println("Ket qua d: " + d);
    }
}
