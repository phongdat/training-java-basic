package chap01.basic;

public class p08_condition {
    public static void main(String[] args) {
        int number = 16;
        if(number % 2 == 0) {
            System.out.println("so chan");
        } else {
            System.out.println("so le");
        }
        /*
            //cach  2
            int number = 10;
            String result = "so chan";
            if(number % 2 == 1) result = "so le";
            System.out.println(result);
         */
    }
}
