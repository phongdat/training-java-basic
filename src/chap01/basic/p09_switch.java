package chap01.basic;

public class p09_switch {
    public static void main(String[] args) {
        int number = 1;
        switch (number) {
            case 2:
                System.out.println("thu 2");
                break;
            case 3:
                System.out.println("thu 3");
                break;
            default:
                System.out.println("chu nhat");
                break;
        }
    }
}
