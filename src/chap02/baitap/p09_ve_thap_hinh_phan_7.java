package chap02.baitap;

public class p09_ve_thap_hinh_phan_7 {
    public static void main(String[] args) {
        final int LENGTH = 9;
        int number = 1;
        while (number <= LENGTH) {
            for (int i = 1 ; i <= LENGTH; i++) {
                if(i >= 2 && i<= LENGTH - 1 && number >= 2 && number <= LENGTH - 1) {
                    System.out.print("  ");
                } else  {
                    System.out.print("* ");
                }
            }
            System.out.println();
            number++;
        }
    }
}
