package chap02.baitap;

import java.util.Scanner;

public class p01_may_tinh_bo_tui {
    public static void main(String[] args) {
        int numberFirst;
        int numberSecond;
        int result = 0;
        String caculate;
        Scanner sc = new Scanner(System.in);
        System.out.println("Number First: ");
        numberFirst = sc.nextInt();
        sc.nextLine();

        System.out.println("Number Second: ");
        numberSecond = sc.nextInt();
        sc.nextLine();

        System.out.println("Caculate: ");
        caculate = sc.nextLine();

        sc.close();
        System.out.println("Number First: " + numberFirst);
        System.out.println("Number Second: " + numberSecond);
        System.out.println("Caculate: " + caculate);
        switch (caculate) {
            case "+":
                result = numberFirst + numberSecond;
                break;
            case "-":
                result = numberFirst - numberSecond;
                break;
            case "x":
            case "*":
                result = numberFirst * numberSecond;
                break;
            case "/":
                result = numberFirst / numberSecond;
                break;
            default:
                break;
        }
        //System.out.println("result: " + result);
        System.out.println("-----------------------");
        System.out.printf("%d %s %d = %d", numberFirst, caculate, numberSecond, result);

    }
}
