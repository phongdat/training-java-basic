package chap02.baitap;

public class p06_ve_thap_hinh_phan_4 {
    public static void main(String[] args) {
        int number = 5;
        while (number >= 1) {
            for (int i = 1; i <= 5 - number; i++) System.out.print("  ");
            for(int j = 1; j <= number; j++) System.out.print("* ");
            System.out.println();
            number--;
        }
    }
}
