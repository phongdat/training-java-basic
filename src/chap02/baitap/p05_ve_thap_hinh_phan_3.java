package chap02.baitap;

public class p05_ve_thap_hinh_phan_3 {

    public static void main(String[] args) {
        int number = 1;
        while (number <= 5) {
            for (int i = 1; i <= 5 - number; i++) System.out.print("  ");
            for (int j = 1; j <= number; j++) System.out.print("* ");
            System.out.println();
            number++;
        }
    }

}
