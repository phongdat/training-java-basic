package chap02.baitap;

import java.util.Scanner;

public class p02_di_tim_an_so {
    public static void main(String[] args) {
        //create random number 1- 100
        final int MAX_NUMBER = 100;
        final int MIN_NUMBER = 1;
        int range = (MAX_NUMBER - MIN_NUMBER) + 1;
        int secretNumber = (int)(Math.random() * range) + MIN_NUMBER;
        System.out.println(secretNumber);

        int yourNumber = 0;
        int score = 0;
        Scanner sc = new Scanner(System.in);
        while (secretNumber != yourNumber) {
            System.out.println("Your's Number (1-100)");
            yourNumber = sc.nextInt();
            if(yourNumber > secretNumber) {
                System.out.println("Greater than!");
            } else if(yourNumber < secretNumber) {
                System.out.println("Less than!");
            } else {
                System.out.println("Success full!");
            }
            score++;
        }
        sc.close();
        System.out.println("Score: " + score);
    }
}
