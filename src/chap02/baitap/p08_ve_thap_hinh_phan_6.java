package chap02.baitap;

public class p08_ve_thap_hinh_phan_6 {
    public static void main(String[] args) {
        /*
        //cach  1
        String record = "";
        String result = "";
        String space = "";
        String left = "";
        String right = "";

        int number = 1;
        while (number <= 5) {
            space = "";
            left = "";
            right = "";
            for (int s = 1; s <= (5 - number); s++) space += " ";
            for(int l = number; l >= 2; l--) left += l;
            for(int r = 2; r <= number; r++) right += r;
            record = space + left + "1" + right + "\n";
            result = result + record;
            number++;
        }
        System.out.println(result);
         */


        /*
        //cach 2
        int c6 = 1;
        while (c6 <= 5) {
            for (int l = 1; l <= 5-c6; l++) System.out.print("  ");
            for (int l = c6; l >= 1; l--) System.out.print(l + " ");
            for (int r = 2; r <= c6; r++) System.out.print(r + " ");
            System.out.println();
            c6++;
        }
         */
        /*
            //cach 3
         */

        int number = 1;
        while (number <= 5) {
            for (int i = 5; i > number; i--)System.out.print(" ");
            for (int j = number; j >= 1; j--)System.out.print(j);
            for (int k = 2; k <= number; k++)System.out.print(k);
            System.out.println();
            number++;
        }

    }
}
