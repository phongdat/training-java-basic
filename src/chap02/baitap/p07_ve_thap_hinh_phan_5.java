package chap02.baitap;

public class p07_ve_thap_hinh_phan_5 {
    public static void main(String[] args) {
        int number = 1;
        while (number <= 5) {
            for (int i = 1; i <= number; i++) {
                System.out.print(i + " ");
            }
            System.out.println();
            number++;
        }
    }
}
